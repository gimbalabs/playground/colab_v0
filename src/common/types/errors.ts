export class ErrorType {
  public static Server =
    "Oops.. Looks like there was a problem on our side. Please try again later.";
  public static UserNameTaken =
    "A user with a given user name already exists. Please choose a different one.";
}

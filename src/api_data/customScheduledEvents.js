export const customScheduledEvents = [
  {
    year: 2021,
    months: [
      {
        month: "July",
        totalNumOfEvents: 4,
        days: [
          {
            day: 18,
            scheduledEvents: [
              {
                title: "My Test Event",
                fromTime: "2021-07-18T13:00:00.000Z",
                toTime: "2021-07-18T15:00:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
              {
                title: "My Test Event",
                fromTime: "2021-07-18T15:00:00.000Z",
                toTime: "2021-07-18T15:30:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
              {
                title: "My Test Event",
                fromTime: "2021-07-18T15:30:00.000Z",
                toTime: "2021-07-18T16:00:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
            ],
          },
          {
            day: 19,
            scheduledEvents: [
              {
                title: "My Test Event",
                fromTime: "2021-07-19T15:00:00.000Z",
                toTime: "2021-07-19T16:30:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
            ],
          },
          {
            day: 20,
            scheduledEvents: [
              {
                title: "My Test Event",
                fromTime: "2021-07-20T12:00:00.000Z",
                toTime: "2021-07-20T13:00:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
            ],
          },
          {
            day: 21,
            scheduledEvents: [
              {
                title: "My Test Event 3",
                fromTime: "2021-07-21T12:00:00.000Z",
                toTime: "2021-07-21T13:00:00.000Z",
                organizer: "@bob_marley",
                description: "A very important event that I cannot miss!",
                participants: [
                  "piotr.napierala94@gmail.com",
                  "john@travolta.com",
                ],
              },
            ],
          },
        ],
      },
    ],
  },
];
